﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;



namespace CSharp7Features
{
    class Program
    {
        static void Main(string[] args)
        {
            Runner runner = new Runner();
            runner.Run();
        }
    }

    public class Employee {
        public int Salary { get; set; }
        public int Years { get; set; }

    }

    public class Manager : Employee
    {
        public int NumberManaged { get; set; }


    }

    public class VicePresident : Manager
    {

        public int StockShares { get; set; }
    }

    public class Runner
    {


       
        public void Run() {


            GetTime(out int hour, out int minutes, out int seconds);

            WriteLine($"Get Time Demo: {hour}:{minutes}:{seconds}");

            PrintSum(10);
            PrintSum2("10");
            Staffing();

        }

        public void GetTime(out int hour, out int minutes, out int seconds)
        {
            hour = 1;
            minutes = 30;
            seconds = 20;
        }

        public void PrintSum(object o)
        {


            if (o is null) return;

            if (!(o is int i)) return;

            int sum = 0;

            for(int j = 0; j <=i; j++)
            {
                sum += j;
            }

            WriteLine($"Sum is {sum}");
         }


        public void PrintSum2(object o)
        {


            if (o is null) return;

            if (o is int i || o is string s && int.TryParse(s, out i)) {
                int sum = 0;
                for (int j = 0; j <= i; j++)
                {
                    sum += j;
                }

                WriteLine($"Sum is {sum}");
            }

        }

        public void Staffing ()
        {

            Employee theEmployee = new VicePresident();

            theEmployee.Years = 20;
            theEmployee.Salary = 175_000_000;
            (theEmployee as VicePresident).NumberManaged = 200;
            (theEmployee as VicePresident).StockShares = 6000;


            switch(theEmployee)
            {
                case VicePresident vp when (vp.StockShares < 5000):
                    WriteLine($"Junior VP with {vp.StockShares} shares");
                    break;
                case VicePresident vp when (vp.StockShares >= 5000):
                    WriteLine($"Senior VP with {vp.StockShares} shares");
                    break;
                case Manager m:
                    WriteLine($"Manager Salary: {m.Salary}");
                    break;

                case Employee e:
                    WriteLine($"Employee Salary: {e.Salary}");
                    break;



            }


        }




    }
}
